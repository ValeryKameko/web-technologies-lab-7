<?php

include_once __DIR__ . '/src/Autoloader.php';

Autoloader::setBaseDirectory(__DIR__ . '/src');
Autoloader::addPrefix('TemplateEngine', 'TemplateEngine');
Autoloader::addPrefix('Model', 'models');
Autoloader::addPrefix('Controller', 'controllers');
Autoloader::addPrefix('View', 'views');
Autoloader::addPrefix('Database', 'databases');
Autoloader::addPrefix('', '');
Autoloader::register();

$router = new Router();

$router->addRoute('/^\/?info$/', function(...$routeParams) {
    phpinfo();
});

$router->addRoute('/^\/?send_mail_page.*$/', function(...$routeParams) {
    $controller = new Controller\SendMailPageController();
    $controller->handleRoute(...$routeParams);
});

$router->addRoute('/^\/?send_mail.*$/', function(...$routeParams) {
    $controller = new Controller\SendMailController();
    $controller->handleRoute(...$routeParams);
});

$router->addRoute('/^\/?add_user_page.*$/', function(...$routeParams) {
    $controller = new Controller\AddUserPageController();
    $controller->handleRoute(...$routeParams);
});

$router->addRoute('/^\/?add_user.*$/', function(...$routeParams) {
    $controller = new Controller\AddUserController();
    $controller->handleRoute(...$routeParams);
});

$router->addRoute('/^\/?delete_user_page.*$/', function(...$routeParams) {
    $controller = new Controller\DeleteUserPageController();
    $controller->handleRoute(...$routeParams);
});

$router->addRoute('/^\/?delete_user.*$/', function(...$routeParams) {
    $controller = new Controller\DeleteUserController();
    $controller->handleRoute(...$routeParams);
});


$router->route();