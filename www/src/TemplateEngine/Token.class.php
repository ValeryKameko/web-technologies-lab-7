<?php

namespace TemplateEngine;

class Token
{
    private $type;
    private $value;
    private $line;

    public const EOF_TYPE = -1;
    public const TEXT_TYPE = 0;
    public const EXPRESSION_START_TYPE = 1;
    public const EXPRESSION_END_TYPE = 2;
    public const BLOCK_START_TYPE = 3;
    public const BLOCK_END_TYPE = 4;
    public const OPERATOR_TYPE = 5;
    public const STRING_TYPE = 6;
    public const NAME_TYPE = 7;
    public const NUMBER_TYPE = 8;
    public const PUNCTUATION_TYPE = 9;

    public function __construct($type, $value, $line)
    {
        $this->type = $type;
        $this->value = $value;
        $this->line = $line;
    }

    public function __toString()
    {
        $stringType = self::typeToString($this->type);
        $stringValue = $this->value;
        return "{$stringType}[$stringValue]";
    }

    public function test($type, $values = NULL)
    {
        $match = true;
        if ($type !== NULL) {
            $match = $match && (
                (\is_array($type) && \in_array($this->type, $type)) ||
                (\is_int($type) && $this->type === $type)
            );
        }
        if ($values !== NULL) {
            $match = $match && (
                (\is_array($values) && \in_array($this->value, $values)) ||
                (\is_string($values) && $this->value === $values)
            );
        }
        return $match;
    }

    public function getValue() 
    {
        return $this->value;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getTypeString()
    {
        return self::typeToString($this->type);
    }

    public function getLine()
    {
        return $this->line;
    }

    public static function typeToString($type)
    {
        switch ($type) {
            case self::EOF_TYPE:
                $name = 'EOF_TYPE';
                break;
            case self::TEXT_TYPE:
                $name = 'TEXT_TYPE';
                break;
            case self::EXPRESSION_START_TYPE:
                $name = 'EXPRESSION_START_TYPE';
                break;
            case self::EXPRESSION_END_TYPE:
                $name = 'EXPRESSION_END_TYPE';
                break;
            case self::BLOCK_START_TYPE:
                $name = 'BLOCK_START_TYPE';
                break;
            case self::BLOCK_END_TYPE:
                $name = 'BLOCK_END_TYPE';
                break;
            case self::OPERATOR_TYPE:
                $name = 'OPERATOR_TYPE';
                break;
            case self::STRING_TYPE:
                $name = 'STRING_TYPE';
                break;
            case self::NAME_TYPE:
                $name = 'NAME_TYPE';
                break;
            case self::NUMBER_TYPE:
                $name = 'NUMBER_TYPE';
                break;
            case self::PUNCTUATION_TYPE:
                $name = 'PUNCTUATION_TYPE';
                break;
            default:
                throw new \LogicException("Token $type doesn't exist");
        }
        return $name;
    }
}