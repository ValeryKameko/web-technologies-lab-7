<?php

namespace TemplateEngine\Node;

use TemplateEngine\Compiler;
use TemplateEngine\Error\UnimlementedError;

class RawIncludeBlockNode extends Node
{
    public function __construct($rawIncludeExpressionNode, $line)
    {
        parent::__construct([ 'raw_include_expression_node' => $rawIncludeExpressionNode ], [], $line, 'include');
    }

    public function compile(Compiler $compiler)
    {
        $compiler->write('$this->displayRawFile(');
        $this->nodes['raw_include_expression_node']->compile($compiler);
        $compiler->writeLine(', $context);');
    }
}