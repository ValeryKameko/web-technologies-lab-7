<?php

namespace TemplateEngine\Node\Expression\Binary;

use TemplateEngine\Node\Node;
use TemplateEngine\Compiler;
use TemplateEngine\Error\UnimlementedError;
use TemplateEngine\Node\Expression\AbstractExpressionNode;

abstract class AbstractBinaryExpressionNode extends AbstractExpressionNode
{
    public function __construct(Node $left, Node $right, $line)
    {
        parent::__construct(['left' => $left, 'right' => $right], [], $line, '');
    }
    
    public function compile(Compiler $compiler)
    {
        $compiler->write('(');
        $this->nodes['left']->compile($compiler);
        $compiler->write(')');
        $compiler->write(' ');
        $this->compileOperator($compiler);
        $compiler->write(' ');
        $compiler->write('(');
        $this->nodes['right']->compile($compiler);
        $compiler->write(')');
    }
    
    abstract public function compileOperator(Compiler $compiler);
}