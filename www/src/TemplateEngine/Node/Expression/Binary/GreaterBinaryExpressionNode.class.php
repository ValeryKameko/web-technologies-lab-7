<?php

namespace TemplateEngine\Node\Expression\Binary;

use TemplateEngine\Compiler;

class GreaterBinaryExpressionNode extends AbstractBinaryExpressionNode
{
    public function compileOperator(Compiler $compiler)
    {
        $compiler->write('>');
    }
}