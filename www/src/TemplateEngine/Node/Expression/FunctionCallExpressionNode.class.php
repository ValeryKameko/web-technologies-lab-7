<?php

namespace TemplateEngine\Node\Expression;

use TemplateEngine\Error\UnimlementedError;
use TemplateEngine\Compiler;

class FunctionCallExpressionNode extends AbstractExpressionNode
{
    public function __construct($functionName, $parameters, $line)
    {
        parent::__construct([ 'function_name' => $functionName, 'parameters' => $parameters ], [ ], $line, 'function call');
    }

    public function compile(Compiler $compiler)
    {
        $functionName = $this->getNode('function_name')->getAttribute('name');
        $compiler->write("\$context['__functions']['$functionName']");
        $compiler->write('(');

        $first = false;
        foreach ($this->nodes['parameters'] as $parameter) {
            if (!$first)
                $first = true;
            else
                $compiler->write(', ');

            $parameter->compile($compiler);

        }
        $compiler->write(')');
    }
}
