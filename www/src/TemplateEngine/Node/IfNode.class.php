<?php

namespace TemplateEngine\Node;

use TemplateEngine\Compiler;
use TemplateEngine\Error\UnimlementedError;

class IfNode extends Node
{
    public function __construct($ifBlocks, $line, $elseBody = NULL)
    {
        $nodes = [ 'if_blocks' => $ifBlocks ];
        if (!is_null($elseBody))
            $nodes['else_body'] = $elseBody;
        parent::__construct($nodes, [ 'has_else' => !is_null($elseBody) ], $line, 'if');
    }

    public function compile(Compiler $compiler)
    {
        $isFirstBlock = true;

        foreach ($this->nodes['if_blocks'] as $ifBlock) {
            if ($isFirstBlock) {
                $isFirstBlock = false;
                $compiler->write('if (');
            } else {
                $compiler->write('elseif (');
            }
            $ifBlock['expression']->compile($compiler);
            $compiler->write('):');
            $compiler->endLine();
    
            $compiler->indent();
            $ifBlock['body']->compile($compiler);
            $compiler->outdent();
        }

        if ($this->attributes['has_else']) {
            $compiler->write('else:');
            $compiler->endLine();

            $compiler->indent();
            $this->nodes['else_body']->compile($compiler);
            $compiler->outdent();
        }
        $compiler->write('endif;');
        $compiler->endLine();
    }
}