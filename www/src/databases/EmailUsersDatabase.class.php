<?php

namespace Database;

class EmailUsersDatabase extends Database {
    private static $self;

    protected function __construct() {
        parent::__construct('mysql-server', '3306', 'user', 'passwd', 'email-users');
    }

    public static function getDatabase() {
        if (!isset($self)) 
            $self = new EmailUsersDatabase();
        return $self;
    }
}