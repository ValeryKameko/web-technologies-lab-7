<?php

namespace Controller;

use Database\EmailUsersDatabase;
use View\DeleteUserPageView;
use Model\UserModel;

class DeleteUserPageController {
    private $database;
    private $userModel;

    public function __construct() {
        $this->database = EmailUsersDatabase::getDatabase();
        $this->userModel = new UserModel($this->database->getPDO());
    }

    public function handle($options) {
        $users = $this->userModel->getAllUsers();
        $view = new DeleteUserPageView();   
        $view->render([
            'users' => $users,
            'back_url' => urlencode('/delete_user_page'),
            'message_result' => $options['result'] ?? '',
            'message_reason' => $options['reason'] ?? 'Ok',
            'nav_items' => [
                [
                    'link' => '/send_mail_page',
                    'name' => 'Send Mail'
                ],
                [
                    'link' => '/add_user_page',
                    'name' => 'Add User'
                ],
                [
                    'link' => '/delete_user_page',
                    'name' => 'Delete User'
                ]
            ]
        ]);
    }

    public function handleRoute(...$routeParams) {
        $this->handle($_GET);
    }
}