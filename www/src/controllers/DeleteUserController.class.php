<?php

namespace Controller;

use Database\EmailUsersDatabase;
use Model\UserModel;

class DeleteUserController extends Controller {
    private $database;
    private $userModel;
    private $sendMailModel;

    public function __construct() {
        $this->database = EmailUsersDatabase::getDatabase();
        $this->userModel = new UserModel($this->database->getPDO());
    }

    public function handle($options) {
        $userIds = $options['users'];
        $backurl = $options['backurl'];

        if (!is_string($backurl) ||
            !is_array($userIds)) {
                $this->sendError($backurl, 'invalid parameters');
        }

        $userIds = filter_var($userIds, FILTER_VALIDATE_INT, [
            'flags'   => FILTER_REQUIRE_ARRAY,
            'options' => array('min_range' => 1),
        ]);

        $userIds = array_filter($userIds, function ($element) { return !empty($element); });

        if (empty($userIds))
            $this->sendWarning($backurl, 'no users to delete');

        $result = $this->userModel->removeUsers($userIds);

        if (!$result)
            $this->sendError($backurl, 'database error');
        $this->sendOk($backurl);
    }

    public function handleRoute(...$routeParams) {
        $this->handle([
            'users' => $_POST['delete_users'] ?? null,
            'backurl' => $_GET['backurl'] ?? null,
        ]);
    }
}