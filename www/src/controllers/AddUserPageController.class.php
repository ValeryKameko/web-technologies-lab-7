<?php

namespace Controller;

use Database\EmailUsersDatabase;
use View\AddUserPageView;

class AddUserPageController {
    private $database;

    public function __construct() {
        $this->database = EmailUsersDatabase::getDatabase();
    }

    public function handle($options) {
        $view = new AddUserPageView();
        $view->render([
            'back_url' => urlencode('/add_user_page'),
            'message_result' => $options['result'] ?? '',
            'message_reason' => $options['reason'] ?? 'Ok',
            'nav_items' => [
                [
                    'link' => '/send_mail_page',
                    'name' => 'Send Mail'
                ],
                [
                    'link' => '/add_user_page',
                    'name' => 'Add User'
                ],
                [
                    'link' => '/delete_user_page',
                    'name' => 'Delete User'
                ]
            ]
        ]);
    }

    public function handleRoute(...$routeParams) {
        $this->handle($_GET);
    }
}