<?php

namespace Controller;

abstract class Controller {
    public function __construct() {

    }

    public abstract function handleRoute(...$routeParams);

    public static function redirect($url) {
        header("Location: $url");
        exit;
    }

    public static function notFound() {
        http_response_code(404);
        exit;
    }

    protected function redirectWithOptions($rawUrl, $options) {
        $url = $rawUrl;
        $params = '';

        foreach ($options as $key => $value) {
            if (!empty($params))
                $params .= '&';
            $params .= "$key=" . urlencode($value);
        }
        if (!empty($params))
            $url .= "?$params";
        Controller::redirect($url);
    }

    protected function sendError($backurl, $reason) {
        Controller::redirectWithOptions($backurl, [
            'result' => 'error',
            'reason' => $reason,
        ]);
    }

    protected function sendWarning($backurl, $reason) {
        Controller::redirectWithOptions($backurl, [
            'result' => 'warning',
            'reason' => $reason,
        ]);
    }

    protected function sendOk($backurl) {
        Controller::redirectWithOptions($backurl, [
            'result' => 'ok',
        ]);
    }

}