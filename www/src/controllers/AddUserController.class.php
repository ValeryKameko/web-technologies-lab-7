<?php

namespace Controller;

use Database\EmailUsersDatabase;
use Model\UserModel;

class AddUserController extends Controller {
    private $database;
    private $userModel;
    private $sendMailModel;

    public function __construct() {
        $this->database = EmailUsersDatabase::getDatabase();
        $this->userModel = new UserModel($this->database->getPDO());
    }

    public function handle($options) {
        $name = $options['name'];
        $email = $options['email'];
        $backurl = $options['backurl'];

        if (!is_string($backurl) ||
            !is_string($email) || 
            !is_string($name)) {
                $this->sendError($backurl, 'invalid parameters');
        }

        if (empty($email))
            $this->sendError($backurl, 'Email field is empty');
        if (empty($name))
            $this->sendError($backurl, 'Name field is empty');

        $result = $this->userModel->addUser($name, $email);
        if (!$result) {
            $this->sendError($backurl, 'Database error');
        }

        $this->sendOk($backurl);
    }

    public function handleRoute(...$routeParams) {
        $this->handle([
            'name' => $_POST['user_name'] ?? null,
            'email' => $_POST['user_email'] ?? null,
            'backurl' => $_GET['backurl'] ?? null,
        ]);
    }
}