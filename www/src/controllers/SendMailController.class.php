<?php

namespace Controller;

use Database\EmailUsersDatabase;
use Model\UserModel;
use Model\SendMailModel;

class SendMailController extends Controller {
    private $database;
    private $userModel;
    private $sendMailModel;

    public function __construct() {
        $this->database = EmailUsersDatabase::getDatabase();
        $this->userModel = new UserModel($this->database->getPDO());
        $this->sendMailModel = new SendMailModel();
    }

    public function handle($options) {
        $userIds = $options['users'];
        $backurl = $options['backurl'];
        $title = $options['title'];
        $message = $options['message'];

        if (!is_array($userIds) || 
            !is_string($backurl) ||
            !is_string($title) || 
            !is_string($message)) {
                $this->sendError($backurl, 'invalid parameters');
        }

        $userIds = filter_var($userIds, FILTER_VALIDATE_INT, [
            'flags'   => FILTER_REQUIRE_ARRAY,
            'options' => array('min_range' => 1),
        ]);

        $userIds = array_filter($userIds, function ($element) { return !empty($element); });

        $users = $this->userModel->getUsers($userIds);

        if (!is_array($users))
            $this->sendError($backurl, 'database error');
        if (empty($users))
            $this->sendWarning($backurl, 'no users');
        
        foreach ($users as $user) {
            $result = $this->sendMailModel->sendMail($user['email'], $title, $message, [], "");
            if (!$result) {
                $this->sendError($backurl, 'message has not send to all recipients');
            }
        }
        
        $this->sendOk($backurl);
    }

    public function handleRoute(...$routeParams) {
        $this->handle([
            'title' => $_POST['message_title'] ?? null,
            'message' => $_POST['message_text'] ?? null,
            'users' => $_POST['message_users'] ?? null,
            'backurl' => $_GET['backurl'] ?? null,
        ]);
    }
}