<?php

namespace View;

class SendMailPageView extends View {
    public function __construct() {
        parent::__construct();
    }

    public function render($parameters) {
        $template = $this->templateEngineEnvironment->load('send_mail_page.tpl');
        $template->render($parameters);
    }
}