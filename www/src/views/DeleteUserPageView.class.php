<?php

namespace View;

class DeleteUserPageView extends View {
    public function __construct() {
        parent::__construct();
    }

    public function render($parameters) {
        $template = $this->templateEngineEnvironment->load('delete_user_page.tpl');
        $template->render($parameters);
    }
}