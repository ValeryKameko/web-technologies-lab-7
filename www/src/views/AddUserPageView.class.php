<?php

namespace View;

class AddUserPageView extends View {
    public function __construct() {
        parent::__construct();
    }

    public function render($parameters) {
        $template = $this->templateEngineEnvironment->load('add_user_page.tpl');
        $template->render($parameters);
    }
}