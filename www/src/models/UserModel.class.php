<?php

namespace Model;

class UserModel {
    private $db;

    public function __construct($db) {
        $this->db = $db;
    }

    public function addUser($userName, $userEmail) {
        $sql = 'INSERT INTO `email-users`.`user` (`name`, `email`)
                VALUE (:name, :email)';
        $query = $this->db->prepare($sql);
        return $query->execute([
            ':name' => $userName,
            ':email' => $userEmail,
        ]);
    }

    public function getAllUsers() {
        $sql = 'SELECT * 
                FROM `email-users`.`user`';
        $query = $this->db->prepare($sql);
        $query->execute([]);
        return $query->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getUsers($ids) {
        $sql = 'SELECT * 
                FROM `email-users`.`user`
                WHERE id IN (:'. implode(',:', array_keys($ids)) . ')
                ORDER BY id';
        $query = $this->db->prepare($sql);

        foreach ($ids as $key => $id) {
            $query->bindValue(":". $key, $id);
        }

        $query->execute();
        return $query->fetchAll();
    }

    public function removeUsers($ids) {
        $sql = 'DELETE FROM `email-users`.`user`
                WHERE id IN (:'. implode(',:', array_keys($ids)) . ')
                ORDER BY id';
        $query = $this->db->prepare($sql);

        foreach ($ids as $key => $id) {
            $query->bindValue(":". $key, $id);
        }

        return $query->execute();
    }

}