<?php

namespace Model;

class SendMailModel {
    public function __construct() {

    }

    public function sendMail($to, $subject, $message, $headers, $parameters) {
        $headers .= "From: \"bot\" < vkamieko@gmail.com >\n";
        $headers .= "X-Sender: \"dev.lab7.com\" < vkamieko@gmail.com >\n";
        $headers .= 'X-Mailer: PHP/' . phpversion();
        $headers .= "Return-Path: vkamieko@gmail.com\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/plain; charset=utf-8\n";

        $parameters .= '-f "bot@dev.lab7.com"';
        return mail($to, $subject, $message, $headers, $parameters);
    }
}