<section class="mail-form">
    <h2 class="mail-form__title">Send mails</h2>
    <form action="/send_mail?backurl={{ back_url }}" method="POST" id="mail-form__form">
        <div class="row">
            <label for="mail-form__message-title" class="col-25">Title</label>
            <input type="text" name="message_title" id="mail-form__message-title" class="col-75">
        </div>
        <div class="row">
            <label for="mail-form__message-text" class="col-25">Message</label>
            <textarea name="message_text" id="mail-form__message-text" cols="30" rows="10" class="col-75"></textarea>
        </div>
        <div class="row">
            <label for="mail-form__emails-select" class="col-25">Recipients</label>
            <select name="message_users[]" id="mail-form__users-select" class="col-75" multiple>
            {% for user in users %}
                <option value="{{ user['id'] }}">{{ user['name'] }} -- {{ user['email'] }}</option>
            {% endfor %}
            </select>
        </div>
        {% include 'message.tpl' %}
        <div class="row mail-form__button-container">
            <button type="submit" class="send-button">Send message</button>
        </div>
    </form>
</section>
