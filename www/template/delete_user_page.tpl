<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Mail sender</title>
        <link rel="stylesheet" href="/css/delete_user_page.css">
    </head>
    <body>
        {% include 'header.tpl' %}
        <main class="main">
            <div class="main__item">
                {% include 'navigation.tpl' %}
            </div>
            <div class="main__item">
                <div class="main__delete-user-form-container">
                    {% include 'delete_user_form.tpl' %}
                </div>
            </div>
        </main>
        {% include 'footer.tpl' %}
    </body>
</html>