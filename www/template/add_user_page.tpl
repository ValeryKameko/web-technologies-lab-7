<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Mail sender</title>
        <link rel="stylesheet" href="/css/add_user_page.css">
    </head>
    <body>
        {% include 'header.tpl' %}
        <main class="main">
            <div class="main__item">
                {% include 'navigation.tpl' %}
            </div>
            <div class="main__item">
                <div class="main__add-user-form-container">
                    {% include 'add_user_form.tpl' %}
                </div>
            </div>
        </main>
        {% include 'footer.tpl' %}
    </body>
</html>