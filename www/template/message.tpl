{% if (message_result == 'error') %}
<div class="error-message row">
    <p class="error-message__reason">{{ message_reason }}</p>
</div>
{% elseif (message_result == 'warning') %}
<div class="warning-message row">
    <p class="warning-message__reason">{{ message_reason }}</p>
</div>
{% elseif (message_result == 'ok') %}
<div class="ok-message row">
    <p class="ok-message__reason">{{ message_reason }}</p>
</div>
{% endif %}