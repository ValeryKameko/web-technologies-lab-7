<section class="delete-user-form">
        <h2 class="delete-user-form__title">Delete</h2>
        <form action="/delete_user?backurl={{ back_url }}" method="POST" id="delete-user-form__form">
            <div class="row">
                <label for="delete-user-form__emails-select" class="col-25">Users</label>
                <select name="delete_users[]" id="delete_users-form__users-select" class="col-75" multiple>
                {% for user in users %}
                    <option value="{{ user['id'] }}">{{ user['name'] }} -- {{ user['email'] }}</option>
                {% endfor %}
                </select>
            </div>
            {% include 'message.tpl' %}
            <div class="row delete-user-form__button-container">
                <button type="submit" class="send-button">Delete user</button>
            </div>
        </form>
    </section>