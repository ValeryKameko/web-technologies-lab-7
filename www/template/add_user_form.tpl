<section class="add-user-form">
    <h2 class="add-user__title">Add user</h2>
    <form action="/add_user?backurl={{ back_url }}" method="POST" id="add-user-form__form">
        <div class="row">
            <label for="add-user__user-name" class="col-25">Name</label>
            <input type="text" name="user_name" id="add-user__user-name" class="col-75">
        </div>
        <div class="row">
            <label for="add-user__user-email" class="col-25">Email</label>
            <input type="email" name="user_email" id="add-user__user-email" class="col-75">
        </div>
        {% include 'message.tpl' %}
        <div class="row add-user__button-container">
            <button type="submit" class="send-button">Add user</button>
        </div>
    </form>
</section>
