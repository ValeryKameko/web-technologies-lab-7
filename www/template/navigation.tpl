<section class="navigation">
    <nav class="navigation__nav">
        <ul class="navigation__link-container">
            {% for nav_item in nav_items %}
            <li class="navigation__item">
                <a href="{{ nav_item['link'] }}" class="navigation__link myButton">{{ nav_item['name']}}</a>
            </li>
            {% endfor %}
        </ul>
    </nav>
</section>