CREATE DATABASE IF NOT EXISTS `email-users` COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `email-users`.`user` (
    `id` int NOT NULL AUTO_INCREMENT,
    `name` tinytext NOT NULL,
    `email` tinytext NOT NULL,
    PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;