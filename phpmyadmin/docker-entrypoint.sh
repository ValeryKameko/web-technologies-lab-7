#!/bin/sh

cp -RT /usr/share/phpmyadmin /etc/phpmyadmin

addgroup -S www-data 
adduser -S www-data -G www-data 
chown www-data:www-data -R ${PHPMYADMIN_SESSION_PATH}

chown www-data:www-data -R /etc/phpmyadmin

touch /etc/phpmyadmin/config.secret.inc.php
echo $(</dev/urandom tr -dc '12345!@#$%qwertQWERTasdfgASDFGzxcvbZXCVB' | head -c8; echo "") >> /etc/phpmyadmin/config.secret.inc.php

php-fpm7 -F